<?php
class Route
{
    public $route;
    public $code;

    public function __construct()
    {
        $route['code'] = 200;
        $route['route'] = $_SERVER['REQUEST_URI'];
        $url = explode('?', $route['route']);

        if ($url[0] != '/' && $url[0] != null) {
            $route['alias'] = explode('/', trim($url[0], '/'));
            $configuration = array_reverse($route['alias']);

            $dir = scandir(FOLDER_LANGUAGE);
            foreach ($dir as $key => $value) {
                if (!in_array($value,array(".",".."))) {
                    if (is_dir(FOLDER_LANGUAGE . DIRECTORY_SEPARATOR . $value)) {
                        $language[$value] = $value;
                    }
                }
            }

            foreach ($configuration as $identify){
                if(empty($route['language']) && !empty($language[$identify])){
                    $route['language'] = $identify;
                    break;
                }
                if(empty($route['controller']) && file_exists(FOLDER_CONTROLLER . '/' . $identify . 'Controller.php')){
                    $route['controller'] = $identify;
                    continue;
                }
                if(empty($route['method'])){
                    $route['method'] = $identify;
                    continue;
                }
            }
            if(empty($route['language'])){
                $route['language'] =  DEFAULT_LANGUAGE ;
            }
            if(empty($route['controller'])){
                $route['controller'] =  'index';
            }
            if(empty($route['method'])){
                $route['method'] =  'index';
            }

        } else {
            $route['alias'][] = '/';
            $route['method'] = $route['controller'] = 'index';
            $route['language'] =  DEFAULT_LANGUAGE ;
        }

        if(preg_match('/index/i',$url[0])){
            $this->rewrite(301,  preg_replace("/\/{2,}/","/",str_ireplace('index','',$url[0])));
        }

        if($route['language'] == DEFAULT_LANGUAGE && $route['alias'][0] == DEFAULT_LANGUAGE){
            unset($route['alias'][0]);
            $this->rewrite(301, '/' . implode('/',$route['alias']).(!empty($_GET) ? '?' . $_GET : ''));
        }

        $route['get'] = !empty($_GET) ? $_GET : '';

        $valid_url = array();
        if($route['language'] != DEFAULT_LANGUAGE) $valid_url[] =  $route['language'];
        if($route['controller'] != 'index') $valid_url[] = $route['controller'];
        if($route['method'] != 'index' ) $valid_url[] = $route['method'];
        if($url[0] != ('/' . implode('/',  $valid_url))) $route['code'] = 404;

        $this->route = $route;
    }

    protected function getController()
    {
        if (file_exists(FOLDER_CONTROLLER . '/' . $this->route['controller'] . '/' . $this->route['controller'] . 'Controller.php')) {
            return FOLDER_CONTROLLER . '/' . $this->route['controller'] . '/' . $this->route['controller'] . 'Controller.php';
        }
    }

    protected function getModel()
    {
        if (file_exists(FOLDER_MODEL . '/' . $this->thisName() . '/' . $this->thisName() . '.php')) {
            return FOLDER_MODEL . '/' . $this->thisName() . '/' . $this->thisName() . '.php';
        }
    }

    protected function getView()
    {
        if (file_exists(FOLDER_VIEW . '/' . $this->thisName() . '/' . $this->thisName() . '.twig')) {
            return FOLDER_VIEW . '/' . $this->thisName() . '/' . $this->thisName() . '.twig';
        }
    }

    public function thisName(){
        return str_replace('Controller','',get_class($this));
    }

    public function rewrite($code = 302,$url = null){
        if(!$url){
            $url = $this->route['route'];
        }
        header('Location: '.$url,true,$code);
        exit;
    }
}