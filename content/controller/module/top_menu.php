<?php
class top_menu extends Module{
    public function index(){

        $this->loadLanguage();

        $current_url = (explode("/",trim($_SERVER['REQUEST_URI'],"/")));
        $data['current_url'] = array_shift($current_url);

        $data['all_menu'] = !empty($this->setting) ? $this->setting : [] ;
        $data['menu_name'] = $this->language->get('menu_name');
        $data['site_name'] = $this->language->get('site_name');

        return $this->render($data);
    }
}