<?php

class indexModel extends Model
{
    private $image;

    public function getMatchDetail($id, $game){
        if( $game == 'cs:go' ){

            /* is load db json : else */

            $dom = new DOMDocument('1.0', 'UTF-8');
            $html = $this->download('https://csgo-stats.com/match/'.$id,'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0');
            @$dom->loadHTML($html);
            $rows = $dom->getElementsByTagName('tbody');

            $b = $result = 0; $val = [];
            $key_table_name = ['command_a','command_b'];
            $key_name_head = ['name',0,'K','D','A','+/-','HS%','ADR','MVP','SCORE','HLTV R.'];
                foreach ($rows as $table){
                    $t=0;
                    foreach ($table->childNodes as $tr) {
                        $d=0;
                        foreach ($tr->childNodes as $td) {
                            if($d == 0){
                                foreach($td->getElementsByTagName('img') as $img){
                                    $val[$key_table_name[$b]][$t]['image'] = '<img src="'.$img->getAttribute('src').'"/>';
                                    break;
                                }
                            }
                            $val[$key_table_name[$b]][$t][$key_name_head[$d]]=$td->nodeValue;
                            if($key_name_head[$d] == '+/-'){
                                $result =+ $td->nodeValue;
                            }
                            $d++;
                        }
                        $t++;
                    }
                    $val[$key_table_name[$b]]['result'] = $result;
                    $b++;
                    $result = 0;
                }
            return $val;
        }elseif ($game == 'dota2'){

            /* is load db json : else */

            return [];
        }
    }

    private function download($url,$user_agent) {
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt ($ch, CURLOPT_COOKIEJAR, BASE_DIR.'/cookie.txt');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, BASE_DIR.'/cookie.txt');
        $result = curl_exec ($ch);
        curl_close ($ch);
        return $result;
    }

    public function getMatch($page, $limit, $order, $sort)
    {
        $page = ((int)$page - 1) * $limit;
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "match ORDER BY " . $order . " " . $sort . " LIMIT " . (int)$page . "," . (int)$limit, 1);
    }

    public function getMatchCount()
    {
        return $this->db->query("SELECT COUNT(*) as count FROM " . DB_PREFIX . "match ")->count;
    }

    public function saveTiket($data)
    {
        return $this->db->query("INSERT INTO " . DB_PREFIX . "match SET match_login='". $this->escape($data['match_login']) ."', match_id='". $this->escape($data['match_id'])."', match_game='". $this->escape($data['match_game']) ."', image='".$this->escape($data['image'])."', status=false");
    }

    public function resizeImage($filename, $width = 320, $height = 240)
    {
        $image_info = getimagesize($filename);
        $image_type = $image_info[2];
        if ($image_type == IMAGETYPE_JPEG) {
            $old_image = imagecreatefromjpeg($filename);
        } elseif ($image_type == IMAGETYPE_GIF) {
            $old_image = imagecreatefromgif($filename);
        } elseif ($image_type == IMAGETYPE_PNG) {
            $old_image = imagecreatefrompng($filename);
        }
        $new_image = imagecreatetruecolor($width, $height);
        $whiteBackground = imagecolorallocate($new_image, 255, 255, 255);
        imagefill($new_image, 0, 0, $whiteBackground);
        imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $width, $height, imagesx($old_image), imagesy($old_image));;
        $this->image = $new_image;
        $this->saveImage($filename);
    }

    private function saveImage($filename, $image_type = IMAGETYPE_JPEG, $compression = 75, $permissions = null)
    {
        if ($image_type == IMAGETYPE_JPEG) {
            imagejpeg($this->image, $filename, $compression);
        } elseif ($image_type == IMAGETYPE_GIF) {
            imagegif($this->image, $filename);
        } elseif ($image_type == IMAGETYPE_PNG) {
            imagepng($this->image, $filename);
        }
        if ($permissions != null) {
            chmod($filename, $permissions);
        }
    }

}