<?php
error_reporting(E_ALL);
define('BASE_DIR',__DIR__);
define('FOLDER_CONTROLLER','content/controller');
define('FOLDER_MODEL', 	   'content/model');
define('FOLDER_VIEW', 	   'content/view');
define('FOLDER_LANGUAGE',  'content/language');
define('FOLDER_SYSTEM',    'system');
define('FOLDER_CACHE',     'system/cache');
define('DEFAULT_LANGUAGE', 'ru');

define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASSWORD','');
define('DB_DATABASE','kibersport');
define('DB_PREFIX','g1s_');