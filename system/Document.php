<?php

class Document extends Route
{
    public $docunent = array();

    protected function setTitle($data)
    {
        $this->docunent['title'] = $data;
    }

    protected function setDescription($data = null)
    {
        $this->docunent['description'] = str_replace('"', '', $data);
    }

    protected function setKeywords($data = null)
    {
        $this->docunent['keywords'] = str_replace('"', '', $data);
    }

    protected function setMeta($data, $name)
    {
        $this->docunent['meta'][] = ['name'=>str_replace('"', '\'', $name),'content'=>str_replace('"', '\'', $data)];
    }

    protected function setLink($data)
    {
        $this->docunent['link'][] = $data;
    }

    protected function setScript($data)
    {
        $this->docunent['script'][] = $data;
        foreach ($data as $script) {
            if (!empty($script['src'])) {
                echo '<script src="' . $script['src'] . '" type="text/javascript"></script>';
            } elseif (!empty($script['text'])) {
                echo '<script><!--' . "\n" . $script['text'] . "\n" . '--></script>';
            }
        }
    }

    protected function pagination($count, $page)
    {
        $curent_get = $this->route['route'];
        $all_page = ceil($count / $page) + 1;
        if ($all_page < 3) {
            return;
        }
        $curent_page = isset($_GET['page']) ? (int)$_GET['page'] : 1;

        if ($curent_page > 2) {
            $previous = str_replace('page=' . $curent_page, 'page=' . ($curent_page - 1), $curent_get);
        } else {
            $previous = trim(str_replace('page=' . $curent_page, '', $curent_get), '\?\&');
        }

        if (isset($_GET['page'])) {
            $next = str_replace('page=' . $curent_page, 'page=' . (($curent_page + 1) >= $all_page ? $curent_page : $curent_page + 1), $curent_get);
        } else {
            $next = (!empty($_GET) ? $curent_get . '&page=2' : '?page=2');
        }

        $pagination = '<ul class="pagination"><li class="page-item"><a class="page-link" href="' . ($curent_get ? $previous : '?' . $previous) . '">Previous</a></li>';

        for ($i = 1; $i < $all_page; $i++) {
            if (isset($_GET['page'])) {
                $url = str_replace('page=' . $curent_page, '', $curent_get) . ($i > 1 ? 'page=' . $i : '');
            } elseif (!empty($_GET)) {
                $url = $curent_get . ($i > 1 ? '&page=' . $i : '');
            } else {
                $url = $curent_get . ($i > 1 ? '?page=' . $i : '');
            }
            $url = trim($url, '\?\&');
            $pagination .= '<li class="page-item' . ($curent_page == $i ? ' active' : '') . '"><a class="page-link" href="' . $url . '">' . $i . '</a></li>';
        }
        $pagination .= '<li class="page-item"><a class="page-link" href="' . $next . '">Next</a></li></ul>';

        return $pagination;
    }
}
