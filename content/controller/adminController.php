<?php
class adminController extends Controller
{

    public function index()
    {
        if (empty($_SESSION['admin'])) {
            $this->rewrite(302, '/admin/login');
        }
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/bootstrap.css']);
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/style.css']);
        $this->setScript(['src' => 'js/jquery.js']);
        $this->setScript(['src' => 'js/bootstrap.js']);
        $this->setTitle('Админ панель');

        $data['h2'] = 'Админ панель';
        $data['name'] = $_SESSION['admin_name'];

        $this->loadModel();

        $get = $this->route['get'];

        $data['h2'] = 'Всего тикетов: ' . $this->adminModel->getTiketCount();

        $data['order_name'] = isset($get['name']) && $get['name'] == 'asc' ? 0 : 1;
        $data['order_email'] = isset($get['email']) && $get['email'] == 'asc' ? 0 : 1;
        $data['order_status'] = isset($get['status']) && $get['status'] == 'asc' ? 0 : 1;

        $page = !empty($get['page']) ? $get['page'] : 1;

        $data['pagination'] = $this->pagination($this->adminModel->getTiketCount(), 3);

        if (isset($get['name'])) {
            $order = 'name';
            $sort = $get['name'] == 'asc' ? 'ASC' : 'DESC';
        } elseif (isset($get['email'])) {
            $order = 'email';
            $sort = $get['email'] == 'asc' ? 'ASC' : 'DESC';
        } elseif (isset($get['status'])) {
            $order = 'status';
            $sort = $get['status'] == 'asc' ? 'ASC' : 'DESC';
        } else {
            $order = 'name';
            $sort = 'ASC';
            $get = array('name' => 'asc');
            $data['order_name'] = 0;
        }

        $data['get'] = $get;

        $data['tiket'] = $this->adminModel->getTiket($page, 3, $order, $sort);

        $this->render($data);
    }

    public function login()
    {
        if (!empty($_SESSION['admin']) && $_SESSION['admin'] == true) {
            $this->rewrite(302, '/admin');
        }
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/bootstrap.css']);
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/style.css']);
        $this->setScript(['src' => 'js/jquery.js']);
        $this->setScript(['src' => 'js/bootstrap.js']);
        $this->setTitle('Админ панель Вход');

        $data['h2'] = 'Вход в админ панель';
        $data['error'] = '';

        if (!empty($_POST)) {

            $this->loadModel();

            $data['login']    = $this->adminModel->escape($_POST['login']);
            $data['password'] = $this->adminModel->escape($_POST['password']);

            $user = $this->adminModel->login($data['login'], $data['password']);

            if (!$data['login'] || !$data['password'] || !$user) {
                $data['error'] = 'Неверный логин или пароль';
            } else {
                $_SESSION['admin'] = true;
                $_SESSION['admin_name'] = $user->login;
                $this->rewrite(302, '/admin');
            }

        }

        $this->render($data, 'login.phtml');
    }

    public function edit()
    {

        if (empty($_SESSION['admin'])) {
            $this->rewrite(302, '/admin/login');

        }
        if (empty($_GET['id'])) {
            $this->rewrite(302, '/admin');
        }

        $this->loadModel();
        $once_tiket = $this->adminModel->getTiketId($_GET['id']);

        if (!$once_tiket) {
            $this->rewrite(302, '/admin');
        }

        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/bootstrap.css']);
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/style.css']);
        $this->setScript(['src' => 'js/jquery.js']);
        $this->setScript(['src' => 'js/bootstrap.js']);
        $this->setTitle('Админ панель Редактировать Тикет');

        $data['h2'] = 'Редактировать тикет: name: ' . $once_tiket->name;
        $data['InputStatus'] = $once_tiket->status;
        $data['InputText'] = $once_tiket->content;

        if (!empty($_SESSION['save'])) {
            $data['save'] = 1;
            unset($_SESSION['save']);
        } else {
            $data['save'] = 0;
        }

        if (!empty($_POST)) {
            $save_data = array(
                'id' => $once_tiket->id,
                'text' => $this->adminModel->inputText($_POST['InputText']),
                'status' => (int)$_POST['InputStatus']
            );
            $this->adminModel->saveTiket($save_data);
            $_SESSION['save'] = true;
            $this->rewrite();
        }

        $this->render($data, 'edit.phtml');
    }

    public function logout()
    {
        if (!empty($_SESSION['admin'])) {
            unset($_SESSION['admin'], $_SESSION['admin_name']);
        }
        $this->rewrite(302, '/admin/login');
    }
}