<?php

class notfoundController extends Controller
{

    public function index()
    {
        $this->loadLanguage();
        $this->setTitle($this->language->get('title'));
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/bootstrap.min.css']);
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/style.css']);
        $this->setScript(['src' => 'js/jquery.js']);
        $this->setScript(['src' => 'js/bootstrap.min.js']);
        $this->setScript(['src' => 'js/bootstrap.bundle.min.js']);

        $data['not_page_title'] = $this->language->get('not_page_title');
        $data['not_page_text'] = $this->language->get('not_page_text');
        $data['not'] = $this->language->get('not');

        $data['top_menu'] = $this->loadModule('top_menu');

        $this->render($data);
    }
}