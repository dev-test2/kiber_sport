<?php

class indexController extends Controller
{

    public $isValid;

    public function index()
    {
        $this->loadLanguage();

        $this->setTitle($this->language->get('title'));
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/bootstrap.min.css']);
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/style.css']);
        $this->setScript(['src' => 'js/jquery.js']);
        $this->setScript(['src' => 'js/bootstrap.min.js']);
        $this->setScript(['src' => 'js/bootstrap.bundle.min.js']);

        $this->loadModel();
        $get = $this->route['get'];

        $data['math_count'] = $this->indexModel->getMatchCount();

        $data['order_name'] = isset($get['name']) && $get['name'] == 'asc' ? 0 : 1;
        $data['order_game'] = isset($get['game']) && $get['game'] == 'asc' ? 0 : 1;
        $data['order_status'] = isset($get['status']) && $get['status'] == 'asc' ? 0 : 1;

        $page = !empty($get['page']) ? $get['page'] : 1;

        $data['pagination'] = $this->pagination($this->indexModel->getMatchCount(), 3);

        if (isset($get['name'])) {
            $order = 'match_login';
            $sort = $get['name'] == 'asc' ? 'ASC' : 'DESC';
        } elseif (isset($get['game'])) {
            $order = 'match_game';
            $sort = $get['game'] == 'asc' ? 'ASC' : 'DESC';
        } elseif (isset($get['status'])) {
            $order = 'status';
            $sort = $get['status'] == 'asc' ? 'ASC' : 'DESC';
        } else {
            $order = 'match_login';
            $sort = 'ASC';
            $get = array('name' => 'asc');
            $data['order_name'] = 0;
        }

        $data['get'] = $get;

        $data['match'] = $this->indexModel->getMatch($page, 3, $order, $sort);

        foreach ($data['match'] as &$val){
            $val->content = $this->indexModel->getMatchDetail($val->match_id,$val->match_game);

            if($val->content['command_a']['result'] > $val->content['command_b']['result']){
                $val->content['command_a']['result'] = $this->language->get('result_command_true') ;
                $val->content['command_b']['result'] = $this->language->get('result_command_false');
            }elseif($val->content['command_a']['result'] == $val->content['command_b']['result']){
                $val->content['command_a']['result'] = $this->language->get('result_command_none');
                $val->content['command_b']['result'] = $this->language->get('result_command_none');
            }else{
                $val->content['command_a']['result'] = $this->language->get('result_command_false');
                $val->content['command_b']['result'] = $this->language->get('result_command_true');
            }

        }

        /*
            https://api.opendota.com/api/matches/3803990937
            https://csgo-stats.com/match/CSGO-3t5KK-Dy2AY-N9cE7-4fno7-LpphH
        */

        $data['top_menu'] = $this->loadModule('top_menu');

        $this->render($data,'index_template.twig');
    }

    public function newmatch()
    {
        $this->setTitle('Создать новый матч');
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/bootstrap.min.css']);
        $this->setLink(['rel' => 'stylesheet', 'href' => 'style/style.css']);
        $this->setScript(['src' => 'js/jquery.js']);
        $this->setScript(['src' => 'js/bootstrap.min.js']);
        $this->setScript(['src' => 'js/bootstrap.bundle.min.js']);

        $data['h2'] = 'Создать новый матч';

        $data['top_menu'] = $this->loadModule('top_menu');

        $data['error_name']  = '';
        $data['error_match'] = '';
        $data['error_game']  = '';

        if(isset($_POST['InputName'])){
            $data['InputName']  = $_POST['InputName'];
        }else{
            $data['InputName']  = '';
        }
        if(isset($_POST['InputMatch'])){
            $data['InputMatch'] = $_POST['InputMatch'];
        }else{
            $data['InputMatch'] = '';
        }
        if(isset( $_POST['InputGame'])){
            $data['InputGame'] = $_POST['InputGame'];
        }else{
            $data['InputGame'] = '';
        }
        if(isset( $_POST['InputImage'])){
            $data['InputImage'] = $_POST['InputImage'];
        }else{
            $data['InputImage'] = '';
        }

        if(isset( $_POST['InputImage'])){}

        if(!empty($_POST)){

            $this->loadModel();

            $data['error_name']  = $this->validate('name',$data['InputName'],3,10);
            $data['error_match'] = $this->validate('id_match',$data['InputMatch'],5,50);
            $data['error_game']  = $this->validate('text',$data['InputGame'],1,50);

            if(!$data['error_name'] && !$data['error_match'] && !$data['error_game']){
                $save_data = array(
                    'match_login' => $data['InputName'],
                    'match_id'=> $data['InputMatch'],
                    'match_game' => $data['InputGame'],
                    'image'=> $data['InputImage']
                );
                $this->indexModel->saveTiket($save_data);
                unset($_SESSION['id']);
                $this->rewrite(302,'/');
            }
        }

        $this->render($data, 'newmatch.twig');
    }

    public function upload()
    {
        $valid_types = ['gif', 'jpg', 'png', 'jpeg'];

        if (isset($_FILES["file"])) {
            if (is_uploaded_file($_FILES['file']['tmp_name'])) {

                $filename = $_FILES['file']['tmp_name'];
                $extension = substr($_FILES['file']['name'], 1 + strrpos($_FILES['file']['name'], '.'));

                if (!in_array($extension, $valid_types)) {
                    die(json_encode(['error' => 'Неверный формат файла']));
                } else {
                    $new_name = 'image/' . $_SESSION['id'] . '.' . $extension;
                    if (move_uploaded_file($filename, $new_name)) {

                        $this->loadModel();
                        $this->indexModel->resizeImage($new_name);
                        unset($_SESSION['id']);
                        die(json_encode(['success' => $new_name]));

                    } else {
                        die(json_encode(['error' => 'Не удалось загрузить файл']));
                    }
                }
            } else {
                die(json_encode(['error' => 'Не удалось загрузить файл']));
            }
        } else {
            die(json_encode(['error' => 'Не удалось загрузить файл']));
        }
    }

    private function validate($type, $val, $min = 3, $max = 100){
        switch ($type){
            case 'email':
                if (!filter_var($val, FILTER_VALIDATE_EMAIL)){
                    return 'Email адрес указан неверно';
                }
                break;
            case 'text':
                if(mb_strlen($val,'UTF-8') < $min ){
                    return 'Текст должна быть больше '. $min .' символов';
                }
                if(mb_strlen($val,'UTF-8') > $max ){
                    return 'Текст не должна перевышать '. $max .' символов';
                }
                break;
            case 'name':
                if(mb_strlen($val,'UTF-8') < $min ){
                    return 'Имя должна быть больше '. $min .' символов';
                }
                if(mb_strlen($val,'UTF-8') > $max ){
                    return 'Имя не должна перевышать '. $max .' символов';
                }
                if(preg_match("/[^a-zа-я0-9]+$/iu",$val)){
                    return 'Имя должно состоять только из букв';
                }
                break;
            case 'id_match':
                if(mb_strlen($val,'UTF-8') < $min ){
                    return 'Ид матча должна быть больше '. $min .' символов';
                }
                if(mb_strlen($val,'UTF-8') > $max ){
                    return 'Ид матча не должна перевышать '. $max .' символов';
                }
                break;
            default:
                return false;
        }
    }
}