<?php

$_['title']         = 'Главная';
$_['coun_matches']  = 'Зарегистрированно матчей: ';

$_['command_b']  = 'Команда B';
$_['command_a']  = 'Команда A';
$_['image']      = 'Аватар';
$_['name']       = 'Логин';
$_['K']          = 'Убийств';
$_['D']          = 'Смертей';
$_['A']          = 'А';
$_['+/-']        = 'Cчет';
$_['HS%']        = 'HS%';
$_['ADR']        = 'ADR';
$_['MVP']        = 'MVP';
$_['SCORE']      = 'Гол';
$_['HLTV R.']    = 'HLTV R.';

$_['h2'] = 'this h2 tag';
$_['result_command_true']  = 'Победившая команда';
$_['result_command_false'] = 'Проигравшая команда';
$_['result_command_none']  = 'Ничья';