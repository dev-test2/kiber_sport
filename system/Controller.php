<?php

class Controller extends Document
{

    public $content;

    public $language;

    public function ini()
    {
        if (!$this->getController() || $this->route['code'] == 404) {
            $this->notFound();
        }
        include_once BASE_DIR . '/' . $this->getController();
        $basename = basename($this->getController(), '.php');
        $method = $this->route['method'];
        $controller = new $basename;
        if (method_exists($controller, $method)) {
            session_start();
            if (empty($_SESSION['id'])) {
                $_SESSION['id'] = md5(date('G:i:s') . rand(1, 999));
            }
            $controller->$method();
            return;
        } else {
            $this->notFound();
            include_once BASE_DIR . '/' . $this->getController();
            $basename = basename($this->getController(), '.php');
            $controller = new $basename;
            $controller->index();
            return;
        }
        exit('error: method not found');
    }

    private function notFound()
    {
        header("HTTP/1.0 404 Not Found");
        $this->route['controller'] = 'notfound';
        $this->route['method'] = 'index';
    }

    protected function render($data, $template = null)
    {
        $this->printDocument('head.twig', $this->docunent , BASE_DIR .'/'. FOLDER_VIEW .'/index');

        if (!empty($data)) {
            if (!empty($this->language->data)) {
                $data = array_merge($data, $this->language->data);
            }
        } else {
            $data = [];
        }

        if ($this->getView() && !$template) {
            $this->printDocument($this->getView(), $data);
        } elseif ($template) {

            if (file_exists(FOLDER_VIEW . '/' . $this->route['controller'] . '/' . $template)) {
                $this->printDocument($template, $data);
            } else {
                echo 'no view file template';
            }
        } else {
            echo 'no view file';
        }
        $this->printDocument('footer.twig', $data, FOLDER_VIEW .'/index');
    }

    private function printDocument($template, $data, $path = 0)
    {
        $path = !$path ? ( BASE_DIR .'/'. FOLDER_VIEW . '/' . $this->route['controller']) :  $path;
        $loader = new Twig_Loader_Filesystem($path);

        $twig = new Twig_Environment($loader, array(
           //'cache' => BASE_DIR . '/' . FOLDER_CACHE
        ));

        echo $twig->render($template, $data);
    }

    protected function loadModel()
    {
        include_once(BASE_DIR . '/' . $this->getModel());
        $basename = basename($this->getModel(), '.php');
        $baseMethod = $basename . 'Model';
        $this->$baseMethod = new $baseMethod;
    }

    protected function loadLanguage()
    {
        $this->language = new Language($this->route['language'], $this->thisName());
    }

    protected function loadModule($name)
    {
        include_once(BASE_DIR . '/' . FOLDER_CONTROLLER . '/module/' . $name . '.php');
        $this->$name = new $name($this->route['language']);
        return $this->$name->index();
    }

}