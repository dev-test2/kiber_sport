<?php
class Module {

    public $db;
    public $name;
    public $setting;
    public $language;
    public $current_language;

    public function __construct($language)
    {
        $this->name = get_class($this);
        $this->current_language = $language;
        $this->db = new DataBase();

        $value = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE type='". $this->name ."'" );
        if(!empty($value->value)){
            $value = json_decode( $value->value , true );
            $this->setting = $value[$this->current_language];
        }
    }

    public function render($data){
        if(!empty($data)){
            extract($data, EXTR_OVERWRITE);
        }
        ob_start();
        include_once(BASE_DIR .'/'. FOLDER_VIEW . '/module/' . $this->name  . '.twig');
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    protected function loadLanguage(){
        $this->language = new Language( $this->current_language , 'module/' . $this->name);
    }
}
