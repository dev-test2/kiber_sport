<?php
class Language {

    public $language;
    public $controller;
    public $data = [];

    public function __construct($language,$controller)
    {
        $this->language = $language;
        $this->controller = $controller;
        if(file_exists(FOLDER_LANGUAGE . '/' . $this->language . '/' . $this->controller . '.php')){
            include BASE_DIR . '/' . FOLDER_LANGUAGE . '/' . $this->language . '/' . $this->controller . '.php';
            if(!empty($_) && empty($this->data)) {
                foreach ($_ as $key => $val) {
                    $this->data[$key] = $val;
                }
            }
        }
    }

    public function get($key){
         return empty($this->data[$key]) ? $key : $this->data[$key] ;
    }
}
