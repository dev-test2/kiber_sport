<?php

include_once 'config.php';

require_once 'system/autoload.php';

spl_autoload_register(function ($class) {
    include BASE_DIR .'/'. FOLDER_SYSTEM . '/' . $class . '.php';
});

$run = new Controller();
$run->ini();