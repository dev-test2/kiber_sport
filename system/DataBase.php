<?php

class DataBase
{
    private $data_base;
	
    public function __construct()
    {
		if (DB_HOST != '' && DB_USER != '' && DB_DATABASE != '') {
			$this->data_base = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
			$this->data_base->set_charset("utf8");
			if ($this->data_base->connect_errno) {
				echo $this->data_base->connect_error;
			}
		} else {
            echo "Невозможно выполнить запрос к БД так как записть отсутствует в конфиигурации";
        }
    }

    public function escape($string){
        return $this->data_base->real_escape_string($string);
    }

    public function query($arg, $array_once = false)
    {
            $result =  $this->data_base->query($arg);
            $resurse = '';
            if (is_object($result) && $result->num_rows > 1) {
                while ($row = $result->fetch_assoc()) {
                    $resurse[] = (Object)$row;
                }
            }
            if (is_object($result) && $result->num_rows == 1) {
                $array_once ? $resurse[] = (Object)$result->fetch_assoc() : $resurse = (Object)$result->fetch_assoc();
            }
            if (!$resurse) {
                $array_once ? $resurse = array() : $resurse = $this->data_base->insert_id;
            }
            
            return $resurse;
        
    }	
	
	public function __destruct(){
		$this->data_base->close();
	}
	
}